
# README

## Itamaeを使ったサーバプロビジョニング

`前提条件`: 事前に各マシンに公開鍵を配布しておく

### Sparkクライアントノード

```
$ itamae ssh -h [hostname(spark-client)] cookbooks/spark_client_recipe.rb -y spark_set.yml
```

### Sparkマスターノード

```
$ itamae ssh -h [hostname(spark-master)] cookbooks/spark_master_recipe.rb -y spark_set.yml
```

### Sparkワーカーノード

```
$ itamae ssh -h [hostname(spark-worker)] cookbooks/spark_worker_recipe.rb -y spark_set.yml
```

## (備忘録)Vagrantで起動できるようにもした。

```
$ cd sandobox

# Vagrant Boxの起動
$ vagrant up

# 起動できているか確認
$ vagrant status

## 鍵認証でログインできるようにする
$ vagrant ssh-config >> ~/.ssh/config
```

## (備忘録)Itamaeのインストール

`前提条件`:
Ruby2以上がインストールされていること。インストールされていない場合は、http://qiita.com/shimoju/items/41035b213ad0ac3a979eを参考にインストールする

```
$ ruby -v
ruby 2.3.0p0 (2015-12-25 revision 53290) [x86_64-darwin16]

$ gem install itamae
```

## (備忘録の備忘録) Ubuntu環境にVirtualbox,Vagrantをインストールする方法

### Virtualboxインストール

```
sudo vi /etc/apt/sources.list

#最後に以下を追加
deb http://download.virtualbox.org/virtualbox/debian raring contrib

$ sudo wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
$ sudo apt-get update
$ sudo apt-get install virtualbox
```

### Vagrantのインストール

```
wget https://dl.bintray.com/mitchellh/vagrant/vagrant_1.7.2_i686.deb
sudo dpkg --install vagrant_1.7.2_i686.deb
```
