include_recipe './common_inst_recipe.rb'

# sparkのダウンロード
execute "download spark-2.1.1" do
  command "cd /tmp &&
  wget http://d3kbcqa49mib13.cloudfront.net/spark-2.1.1-bin-hadoop2.7.tgz &&
  sudo tar xvfz /tmp/spark-2.1.1-bin-hadoop2.7.tgz -C /opt/ &&
  sudo ln -s /opt/spark-2.1.1-bin-hadoop2.7 /opt/spark &&
  rm -rf /tmp/spark-2.1.1-bin-hadoop2.7.tgz"
  not_if "test -d /opt/spark"
end

# 環境変数追加
remote_file "/etc/profile.d/spark.sh" do
  source "files/spark.sh"
end

# hdfsクライアント、YARNクライアントインストール
package "hadoop-hdfs"
package "hadoop-yarn"

# spark-env.shを追加
remote_file "/opt/spark/conf/spark-env.sh"

# ここから共通設定
include_recipe './common_set_recipe.rb'
