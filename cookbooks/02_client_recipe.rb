# HDFS起動後にHDFSコマンドでディレクトリを作る
execute "sudo -u hdfs hdfs dfs -mkdir -p /hadoop/tmp && sudo -u hdfs hdfs dfs -chmod 777  /hadoop/tmp" do
  not_if "sudo -u hdfs hdfs dfs -ls /hadoop/tmp"
end
execute "sudo -u hdfs hdfs dfs -mkdir -p /tmp && sudo -u hdfs hdfs dfs -chmod 777 /tmp" do
  not_if "sudo -u hdfs hdfs dfs -ls /tmp"
end
execute "sudo -u hdfs hdfs dfs -mkdir -p /hadoop/yarn/app-logs && sudo -u hdfs hdfs dfs -chmod 777  /hadoop/yarn/app-logs" do
  not_if "sudo -u hdfs hdfs dfs -ls /hadoop/yarn/app-logs"
end
execute "sudo -u hdfs hdfs dfs -mkdir -p /pocdata && sudo -u hdfs hdfs dfs -chmod 777  /pocdata" do
  not_if "sudo -u hdfs hdfs dfs -ls /pocdata"
end
