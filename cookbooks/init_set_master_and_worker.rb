User = "hadoop"
# ぱすわーどはhadoop
Pass = "$6$kBt3hzLQUPBpqG8s$sBiQMYBLRSGuiwC3S.BnfhusawvJAMeb52zqrM0Ne1/qH8JgPXobbEmgvdkWGKtjkys2Ze8I8Q6Z0oSMCyA7B/"
Group = "hadoop"

service "network" do
  action :restart
end

group Group do
  action :create
end

user User do
  home "/usr/hadoop"
  password Pass
  gid Group
end

directory "/usr/hadoop" do
  action :create
  mode "755"
end

directory "/usr/hadoop/.ssh" do
   action :create
   mode "700"
   owner User
   group Group
end

# /etc/hostsの配布
template "/etc/hosts" do
  owner "root"
  group "root"
  mode "744"
end

# 必要なライブラリのインストール
%w(wget vim).each do |pkg|
  package pkg
end

# Java8のインストール
execute "curl -LO -H 'Cookie: oraclelicense=accept-securebackup-cookie' 'http://download.oracle.com/otn-pub/java/jdk/8u71-b15/jdk-8u71-linux-x64.rpm' && rpm -ivh jdk-8u71-linux-x64.rpm" do
  not_if "test -f /usr/bin/java"
end

# java7はアンインストール
package "java-1.7.0-openjdk" do
  action :remove
end

# Java環境変数の設定
remote_file "/etc/profile.d/java.sh" do
  mode "744"
end

remote_file "/usr/hadoop/.ssh/id_rsa.pub" do
  mode "644"
  owner "hadoop"
  group "hadoop"
end

remote_file "/usr/hadoop/.ssh/id_rsa" do
  mode "600"
  owner "hadoop"
  group "hadoop"
end

remote_file "/usr/hadoop/.ssh/authorized_keys" do
  source "files/id_rsa.pub"
  mode "600"
  owner "hadoop"
  group "hadoop"
end

def return_hosts_list
  hosts_list = ["localhost"]
  # hosts_list << node["master"]["name"]
  # hosts_list << node["client"]["name"]
  # node["worker"].each do |worker|
  #   hosts_list << worker["name"]
  # end
  hosts_list
end

# return_hosts_list.each do |host|
#   execute "ssh -o StrictHostKeyChecking=no #{host} ; exit" do
#     user "hadoop"
#   end
# end

execute "download hadoop from apache" do
  command "curl -O http://ftp.jaist.ac.jp/pub/apache/hadoop/common/hadoop-2.7.1/hadoop-2.7.1.tar.gz &&
   tar zxvf hadoop-2.7.1.tar.gz -C /usr/hadoop --strip-components 1"
  user User
  not_if "test -d /usr/hadoop/etc"
end

remote_file "/etc/profile.d/hadoop.sh" do
  mode "744"
end

# Hadoopディレクトリ作成
directory "/hadoop" do
  action :create
  mode "775"
  owner User
  group Group
end
directory "/hadoop/hdfs" do
  action :create
  mode "775"
  owner User
  group Group
end
directory "/hadoop/hdfs/data" do
  action :create
  mode "775"
  owner User
  group Group
end
directory "/hadoop/hdfs/name" do
  action :create
  mode "775"
  owner User
  group Group
end
directory "/hadoop/yarn" do
  action :create
  mode "775"
  owner User
  group Group
end
directory "/hadoop/tmp" do
  action :create
  mode "777"
end

template "/usr/hadoop/etc/hadoop/yarn-site.xml" do
  owner User
  group Group
  mode "744"
end
template "/usr/hadoop/etc/hadoop/core-site.xml" do
  owner User
  group Group
  mode "744"
end
remote_file "/usr/hadoop/etc/hadoop/hdfs-site.xml" do
  owner User
  group Group
  mode "744"
end
template "/usr/hadoop/etc/hadoop/slaves" do
  owner User
  group Group
  mode "744"
end
