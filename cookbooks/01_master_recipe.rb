include_recipe './common_inst_recipe.rb'

# hdfsクライアント、YARNクライアントインストール
package "hadoop-hdfs-namenode"
package "hadoop-yarn-resourcemanager"
package "hadoop-mapreduce-historyserver"
package "hadoop-yarn-proxyserver"

# ここから共通設定
include_recipe './common_set_recipe.rb'

# HDFSのフォーマット（初期起動時に必要)
execute "format hdfs file system" do
  command "sudo -u hdfs hdfs namenode -format"
  not_if "test -d /hadoop/hdfs/name"
end

# hadoop-hdfs-namenodeの起動
service 'hadoop-hdfs-namenode' do
  action [:start, :enable]
end
