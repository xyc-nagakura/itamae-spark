# 一般ユーザ作成
user "create user" do
  username "pocuser"
  password "pocpass"
  home "/home/pocuser"
end

# ディレクトリ作成
directory "/hadoop" do
  action :create
  mode "775"
end
directory "/hadoop/hdfs" do
  action :create
  mode "775"
  owner "hdfs"
  group "hdfs"
end
directory "/hadoop/yarn" do
  action :create
  mode "775"
  owner "hdfs"
  group "hdfs"
end
directory "/hadoop/tmp" do
  action :create
  mode "777"
end

# HDFSとYARNの環境設定ファイルを配布
remote_file '/etc/hadoop/conf/hdfs-site.xml' do
  owner 'root'
  group 'root'
  mode '644'
end
%w(/etc/hadoop/conf/core-site.xml /etc/hadoop/conf/yarn-site.xml).each do |file|
  template file do
    action :create
    owner 'root'
    group 'root'
    mode '644'
  end
end
