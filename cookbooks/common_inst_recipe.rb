
# 必要なライブラリのインストール
%w(wget java-1.8.0-openjdk-devel.x86_64 vim).each do |pkg|
  package pkg
end

# java7はアンインストール
package "java-1.7.0-openjdk" do
  action :remove
end

# CDHのリポジトリ登録
execute "yum repos add" do
  command "sudo rpm --import http://archive.cloudera.com/cdh5/redhat/7/x86_64/cdh/RPM-GPG-KEY-cloudera &&
  sudo rpm -ivh http://archive.cloudera.com/cdh5/one-click-install/redhat/7/x86_64/cloudera-cdh-5-0.x86_64.rpm"
  not_if "test -f /etc/yum.repos.d/cloudera-cdh5.repo"
end
remote_file "/etc/yum.repos.d/cloudera-cdh5.repo"
execute "yum clean all"

service "network" do
  action [:restart, :enable]
end
# 共通のhostsを配布
template "/etc/hosts"
