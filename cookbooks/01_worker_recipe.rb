include_recipe './common_inst_recipe.rb'

# hdfsクライアント、YARNクライアントインストール
package "hadoop-hdfs-datanode"
package "hadoop-yarn-nodemanager"
package "hadoop-mapreduce"

# ここから共通設定
include_recipe 'common_set_recipe.rb'

# hadoop-hdfs-datanodeの起動
service 'hadoop-hdfs-datanode' do
  action [:start,:enable]
end
