execute "stop-yarn.sh" do
  user "hadoop"
end

execute "stop-dfs.sh" do
  user "hadoop"
end

execute "start-dfs.sh" do
  user "hadoop"
end

execute "start-yarn.sh" do
  user "hadoop"
end
